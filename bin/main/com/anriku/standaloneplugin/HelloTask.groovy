package com.anriku.standaloneplugin

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction


class HelloTask extends DefaultTask{

    @Input String firstName
    @Input String lastName

    @TaskAction
    void greet() {
        println "Hello," + getFirstName() + ":" +  getLastName()
    }
}
