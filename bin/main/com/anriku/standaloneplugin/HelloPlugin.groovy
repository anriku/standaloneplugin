package com.anriku.standaloneplugin

import org.gradle.api.Plugin
import org.gradle.api.Project


class HelloPlugin implements Plugin<Project> {

    static final String EXTENTION_NAME = "helloExtension"

    @Override
    void apply(Project project) {
        project.extensions.create(EXTENTION_NAME, HelloTaskExtension)
        project.task("hello", type: HelloTask) { task ->
            def extension = project.extensions.findByName(EXTENTION_NAME)
            conventionMapping.firstName = { extension.extFirstName }
            conventionMapping.lastName = { extension.extLastName }
        }
    }
}
